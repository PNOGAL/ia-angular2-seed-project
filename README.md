# IA Angular5 Starter

#### Run `npm install -g @angular/cli` then `npm install` to get started


## Required Reading!
New to Angular? [Angular tutorial](https://angular.io/tutorial)

[Official Angular Style Guide](https://angular.io/guide/styleguide) Go to source for all things Angular

New to Angular Material? [Angular Material](https://material.angular.io/compents/categories)

[Angular Material Guides](https://material.angular.io/guides) Set of nice howtos

## Before you start
Consider a lightweight IDE for developing [Visual Studio Code](https://code.visualstudio.com/)
works great with typescript ecosystem.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Running TS Linter

Run `ng lint` to excute typescript linting. `ng lint --fix` will attempt to fix any errors

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Dependances
[Angular](http://angular.io)

[Angular CLI](https://github.com/angular/angular-cli) 

[Angular Material 2](https://material.angular.io)

[UW Style](https://git.doit.wisc.edu/uw-madison-digital-strategy/uw-style)

Have a look at the `package.json` for more a more detailed view.


## Basic project structure
<pre>app
|   |-- app.component.html
|   |-- app.component.scss
|   |-- app.component.spec.ts					# All tests end with .spec.ts
|   |-- app.component.ts
|   |-- app.module.ts  							# Parent app module
|   |-- app.routing.module.ts				    # Primary place to add new routes
|   |-- core
|   |   |-- config.service.ts 					# Used for config vars
|   |   |-- core.module.ts
|   |   |-- data.service.spec.ts				# Designed to mock data
|   |   |-- data.service.ts						# One catch all service for data transmission
|   |   |-- models 								# Placeholder for user created models/interfaces
|   |   |-- module-import-check.ts 				# Prevents the core module from loading twice.
|   |   `-- navigation						    # Shared navigation component
|   |       |-- navigation.component.html
|   |       |-- navigation.component.scss
|   |       |-- navigation.component.spec.ts
|   |       `-- navigation.component.ts
|   |-- home 									# Starter component using the router
|   |   |-- home.component.html
|   |   |-- home.component.scss
|   |   |-- home.component.spec.ts
|   |   `-- home.component.ts 	
|   `-- shared									# App wide shared directory. All modules could have a shared Directory too
|       `-- shared.module.ts  					# Only add Material components here!
|-- assets
|   |-- material-theme.scss  					# Custom Material theme
|   `-- uwstyle				  					# Copy of UWStyle
|       |-- dist
|       |-- fonts
|       `-- images
|-- index.html									# html entry point gets app.component
|-- main.ts
|-- polyfills.ts								# Enables compatibility for older browsers
|-- styles.css									# Could be used for global styles/imports
|-- test.ts										# Required by karma
|-- tsconfig.app.json
|-- tsconfig.spec.json
`-- typings.d.ts

../.angular-cli.json 							# All project config for the angular cli
../tslint.json 									# All project lint settings ex: "ng lint"
../e2e/											# All project end to end tests ex: "ng e2e"
../tsconfig.json 								# Primary typescript config
</pre>

