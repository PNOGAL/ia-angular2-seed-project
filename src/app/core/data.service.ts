import { identifierModuleUrl } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

import { ConfigService } from './config.service';

@Injectable()
export class DataService {
	private api: string;

	constructor(private http: HttpClientModule, private configService: ConfigService) {
		this.api = this.configService.apiUrl;
	}
}
