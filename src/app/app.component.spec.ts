import { TestBed, async } from '@angular/core/testing';
import { Component, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app.routing.module';

const routes: Routes = [
	{ path: '', pathMatch: 'full', redirectTo: 'home', },
	{ path: 'home', component: HomeComponent }
];

@Component({
	template: '<div>lazy-loaded</div>'
  })
  class LazyComponent { }
  
  @NgModule({
	imports: [RouterModule],
	declarations: [LazyComponent]
  })
  class LazyModule { }

describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [RouterModule, RouterTestingModule.withRoutes(routes)],
			schemas: [ NO_ERRORS_SCHEMA ],
			declarations: [
				AppComponent,
				HomeComponent
			],
		}).compileComponents();
	}));
	it('should create the app', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));
	it(`should have as title 'Angular Starter'`, async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app.title).toEqual('app');
	}));
	it('should render title in a h1 tag', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('h1').textContent).toContain('Angular Starter');
	}));
	it('should render title in a router-outlet tag', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('router-outlet').textContent).toBeDefined();
	  }));
});
